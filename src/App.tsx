import React from "react"
import "./App.css"
import Main from "./Components/Main/Main"
import Sidebar from "./Components/Sidebar/Sidebar"
import Header from "./Components/Header/Header"

const App: React.FC = () => {
  return (
    <div className="App">
      <Header />
      <Sidebar />
      <Main />
    </div>
  )
}

export default App
