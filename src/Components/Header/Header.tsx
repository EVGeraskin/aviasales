import React from "react"
import styles from "./Header.module.css"
import logo from "../../assets/Logo.svg"

const Header: React.FC = () => {
  return (
    <div className={styles.header}>
      <img src={logo} alt="none" />
    </div>
  )
}

export default Header
