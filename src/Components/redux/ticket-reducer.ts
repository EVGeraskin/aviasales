import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import ticketAPI, { TicketType } from "../API/ticket"

export enum TransfersFilterEnum {
  TRANSFERS_NO = 0b0001,
  TRANSFERS_ONE = 0b0010,
  TRANSFERS_TWO = 0b0100,
  TRANSFERS_THREE = 0b1000,
  TRANSFERS_ALL = 0b1111,
}

type TicketState = {
  tickets: Array<TicketType>
  searchId: string | null
  ticketsIsLoading: boolean
  ticketTransfersFilter: TransfersFilterEnum
}

const initialState: TicketState = {
  tickets: [],
  searchId: null,
  ticketsIsLoading: false,
  ticketTransfersFilter: TransfersFilterEnum.TRANSFERS_ALL,
}

export const getTickets = createAsyncThunk("ticket/getTickets", async () => {
  const searchIdResponse = await ticketAPI.getSearchId()
  const tickets = {
    tickets: [],
  }

  if (searchIdResponse) {
    let stopSearch = false

    while (!stopSearch) {
      // eslint-disable-next-line no-await-in-loop
      const ticketsResponse = await ticketAPI.getTickets(
        searchIdResponse.searchId
      )
      Object.assign(tickets, ticketsResponse, tickets)
      if (ticketsResponse) stopSearch = ticketsResponse.stop
    }
  }
  return tickets.tickets
})

const ticketSlice = createSlice({
  name: "ticket",
  initialState,
  reducers: {
    addTickets(state, action: PayloadAction<Array<TicketType>>) {
      return { ...state, tickets: action.payload }
    },
    setTicketFilter(state, action: PayloadAction<number>) {
      return { ...state, ticketTransfersFilter: action.payload }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getTickets.fulfilled, (state, action) => {
      return { ...state, ticketsIsLoading: false, tickets: action.payload }
    })
    builder.addCase(getTickets.pending, (state) => {
      return { ...state, ticketsIsLoading: true }
    })
  },
})

export const { setTicketFilter } = ticketSlice.actions

export default ticketSlice.reducer
