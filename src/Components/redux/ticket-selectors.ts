import { createSelector } from "@reduxjs/toolkit"
import { TicketType } from "../API/ticket"
import { RootState } from "./store"

export enum TicketsSortType {
  TICKET_SORT_BY_PRICE,
  TICKET_SORT_BY_DURATION,
  TICKET_SORT_OPTIMAL,
}

export const getTickets = (state: RootState): Array<TicketType> =>
  state.ticket.tickets
export const getTransfersFilter = (state: RootState): number =>
  state.ticket.ticketTransfersFilter

export const getSortedTickets = (type: TicketsSortType) =>
  createSelector(
    getTickets,
    getTransfersFilter,
    (items: Array<TicketType>, transfersFilter: number) => {
      const tickets = [...items].filter((ticket) => {
        /* Convert stops count to bitmask and apply transferFilter mask to tickets  */
        const filtered =
          (1 << ticket.segments[0].stops.length) & transfersFilter &&
          (1 << ticket.segments[1].stops.length) & transfersFilter

        return !!filtered
      })

      if (type === TicketsSortType.TICKET_SORT_BY_PRICE)
        return tickets.sort((a: TicketType, b: TicketType) => a.price - b.price)

      if (type === TicketsSortType.TICKET_SORT_BY_DURATION)
        return tickets.sort((a: TicketType, b: TicketType) => {
          return a.segments[0].duration - b.segments[0].duration
        })

      let maximumPrice = -1
      let maximumDuration = -1

      for (let i = 0; i < tickets.length; i += 1) {
        if (tickets[i].price > maximumPrice) maximumPrice = tickets[i].price
        if (tickets[i].segments[0].duration > maximumDuration)
          maximumDuration = tickets[i].segments[0].duration
      }

      /* Comparation weight of Ticket price 2 times more of weight of Ticket duration */
      return tickets.sort((a: TicketType, b: TicketType) => {
        return (
          a.segments[0].duration / maximumDuration +
          (a.price / maximumPrice) * 2 -
          (b.segments[0].duration / maximumDuration +
            (b.price / maximumPrice) * 2)
        )
      })
    }
  )
