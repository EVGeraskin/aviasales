import { Radio } from "antd"
import React from "react"
import styles from "./Sorter.module.css"
import "antd/lib/radio/style/index.css"
import { TicketsSortType } from "../redux/ticket-selectors"

type SorterProps = {
  sortType: TicketsSortType
  setSortType: (sortType: TicketsSortType) => void
}

interface RadioButtonEventTarget extends EventTarget {
  value: string
}
interface RadioButtonMouseEvent extends React.MouseEvent<HTMLElement> {
  target: RadioButtonEventTarget
}

const Sorter: React.FC<SorterProps> = ({ sortType, setSortType }) => {
  const handleRadioClick = (event: RadioButtonMouseEvent) => {
    const radioValue = event.target.value
    switch (radioValue) {
      case "cheapest": {
        if (sortType !== TicketsSortType.TICKET_SORT_BY_PRICE)
          setSortType(TicketsSortType.TICKET_SORT_BY_PRICE)
        break
      }
      case "fastest": {
        if (sortType !== TicketsSortType.TICKET_SORT_BY_DURATION)
          setSortType(TicketsSortType.TICKET_SORT_BY_DURATION)
        break
      }
      default:
        if (sortType !== TicketsSortType.TICKET_SORT_OPTIMAL)
          setSortType(TicketsSortType.TICKET_SORT_OPTIMAL)
    }
  }

  return (
    <div className={styles.sorter}>
      <Radio.Group
        className={styles.sorterGroup}
        defaultValue="cheapest"
        buttonStyle="solid"
        size="large"
      >
        <Radio.Button
          className={styles.sorterButton}
          value="cheapest"
          onClick={handleRadioClick}
        >
          Самый дешёвый
        </Radio.Button>
        <Radio.Button
          className={styles.sorterButton}
          value="fastest"
          onClick={handleRadioClick}
        >
          Самый быстрый
        </Radio.Button>
        <Radio.Button
          className={styles.sorterButton}
          value="optimal"
          onClick={handleRadioClick}
        >
          Оптимальный
        </Radio.Button>
      </Radio.Group>
    </div>
  )
}

export default Sorter
