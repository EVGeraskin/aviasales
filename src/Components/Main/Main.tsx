import React, { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Button } from "antd"
import { getTickets } from "../redux/ticket-reducer"
import Sorter from "./Sorter"
import Ticket from "./Ticket"
import { getSortedTickets, TicketsSortType } from "../redux/ticket-selectors"
import "antd/lib/button/style/index.css"
import styles from "./Main.module.css"
import Preloader from "../Custom/Preloader"
import { RootState } from "../redux/store"
import { TicketType } from "../API/ticket"

type PaginationProps = {
  tickets: Array<TicketType>
  page: number
  count?: number
  setPage: (page: number) => void
}

const Pagination: React.FC<PaginationProps> = ({
  tickets,
  page,
  count = 5,
  setPage,
}) => {
  const getTicketPage = () => {
    const showedTickets: Array<ReturnType<typeof Ticket>> = []
    for (let i = 0; i < tickets.length; i += 1) {
      if (i > page * count) break
      showedTickets.push(
        <Ticket
          price={tickets[i].price}
          carrier={tickets[i].carrier}
          segments={tickets[i].segments}
        />
      )
    }
    return showedTickets
  }

  const handleButtonClick = () => {
    setPage(page + 1)
  }

  return (
    <div>
      {getTicketPage()}
      {/* Check for more then {count} tickets on the page and this is not last page */}
      {tickets.length > count && tickets.length > page * count && (
        <Button
          className={styles.showMoreButton}
          onClick={handleButtonClick}
          type="primary"
        >
          Показать ещё 5 билетов!
        </Button>
      )}
    </div>
  )
}

const Main: React.FC = () => {
  const [page, setPage] = useState(1)
  const [sortType, setSortType] = useState(TicketsSortType.TICKET_SORT_BY_PRICE)

  const tickets = useSelector(getSortedTickets(sortType))
  const ticketIsLoading = useSelector(
    (state: RootState) => state.ticket.ticketsIsLoading
  )
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getTickets())
  }, [])

  useEffect(() => {
    setPage(1)
  }, [sortType, setSortType])

  return (
    <div className={styles.main}>
      <Sorter sortType={sortType} setSortType={setSortType} />
      {ticketIsLoading ? (
        <Preloader />
      ) : (
        <Pagination tickets={tickets} page={page} setPage={setPage} />
      )}
    </div>
  )
}

export default Main
