import React from "react"
import styles from "./Ticket.module.css"
import { Segment } from "../API/ticket"

export type TicketProps = {
  price: number | null
  carrier: string | null
  segments: Segment[] | null
}

const Ticket: React.FC<TicketProps> = ({ price, carrier, segments }) => {
  const flyDuration = (index: number) => {
    const duration = segments && segments[index].duration

    return (
      <div>
        <div>В пути</div>
        <span>
          {`${duration && (duration / 60).toFixed()}ч ${
            duration && duration % 60
          }м`}
        </span>
      </div>
    )
  }

  const flyTransfers = (flyIndex: number) => {
    const transfers = segments && segments[flyIndex].stops

    return (
      <div>
        <div>
          {transfers?.length ? (
            `${transfers.length} Пересадк${transfers.length > 1 ? "и" : "а"}`
          ) : (
            <div>
              Без пересадок
              <br />
              <br />
            </div>
          )}
        </div>
        <span>
          {transfers?.map(
            (stop, index) =>
              `${stop}${index < transfers?.length - 1 ? ", " : " "}`
          )}
        </span>
      </div>
    )
  }

  return (
    <div className={styles.ticket}>
      <div className={styles.price}>{price} P</div>
      <div className={styles.logo}>
        <img src={`https://pics.avs.io/99/36/${carrier}.png`} alt="none" />
      </div>
      <div className={styles.time}>
        <div>
          {segments && segments[0].origin} -{" "}
          {segments && segments[0].destination}
        </div>
        <span>10:40 - 08:00</span>
        <div>
          {segments && segments[1].origin} -{" "}
          {segments && segments[1].destination}
        </div>
        <span>11:20 - 00:50</span>
      </div>
      <div className={styles.duration}>
        {flyDuration(0)}
        {flyDuration(1)}
      </div>

      <div className={styles.transfers}>
        {flyTransfers(0)}
        {flyTransfers(1)}
      </div>
    </div>
  )
}

export default Ticket
