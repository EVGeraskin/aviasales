import axios, { AxiosResponse } from "axios"
import axiosRetry from "axios-retry"

const axiosInstance = axios.create({
  baseURL: "https://front-test.beta.aviasales.ru",
})
axiosRetry(axiosInstance, {
  retries: 5,
  retryDelay: axiosRetry.exponentialDelay,
})

export type Segment = {
  origin: string
  destination: string
  date: string
  stops: Array<string>
  duration: number
}

export type TicketType = {
  price: number
  carrier: string
  segments: Array<Segment>
}

type TicketsResponse = {
  tickets: Array<TicketType>
  stop: boolean
}

type SearchId = {
  searchId: string
}

const ticketAPI = {
  getSearchId(): Promise<SearchId> {
    return axiosInstance
      .get("/search")
      .then((response: AxiosResponse<SearchId>) => response.data)
  },
  getTickets(searchId: string): Promise<TicketsResponse> {
    return axiosInstance
      .get(`/tickets?searchId=${searchId}`)
      .then((response: AxiosResponse<TicketsResponse>) => response.data)
  },
}
export default ticketAPI
