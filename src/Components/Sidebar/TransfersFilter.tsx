import React, { useState } from "react"
import { Checkbox } from "antd"
import "antd/lib/checkbox/style/index.css"
import { useDispatch } from "react-redux"
import { CheckboxChangeEvent } from "antd/es/checkbox"
import styles from "./TransfersFilter.module.css"
import { setTicketFilter, TransfersFilterEnum } from "../redux/ticket-reducer"

const TransfersFilter: React.FC = () => {
  const [transfersFilter, setTransfersFilter] = useState(
    TransfersFilterEnum.TRANSFERS_ALL
  )
  const dispatch = useDispatch()

  const handleOnChange = (event: CheckboxChangeEvent) => {
    const selectedTransfersFilter = event.target.value
    let dispatchedFilter

    switch (selectedTransfersFilter) {
      case TransfersFilterEnum.TRANSFERS_NO:
      case TransfersFilterEnum.TRANSFERS_ONE:
      case TransfersFilterEnum.TRANSFERS_TWO:
      case TransfersFilterEnum.TRANSFERS_THREE: {
        dispatchedFilter = transfersFilter ^ selectedTransfersFilter
        setTransfersFilter(dispatchedFilter)
        break
      }
      default: {
        dispatchedFilter =
          transfersFilter === TransfersFilterEnum.TRANSFERS_ALL
            ? 0
            : TransfersFilterEnum.TRANSFERS_ALL

        setTransfersFilter(dispatchedFilter)
      }
    }
    dispatch(setTicketFilter(dispatchedFilter))
  }

  return (
    <div className={styles.transfers}>
      <span>Количество пересадок</span>
      <div className={styles.boxes}>
        <Checkbox
          checked={TransfersFilterEnum.TRANSFERS_ALL === transfersFilter}
          value={TransfersFilterEnum.TRANSFERS_ALL}
          className={styles.checkbox}
          onChange={handleOnChange}
        >
          Все
        </Checkbox>
        <br />
        <Checkbox
          checked={!!(transfersFilter & TransfersFilterEnum.TRANSFERS_NO)}
          value={TransfersFilterEnum.TRANSFERS_NO}
          className={styles.checkbox}
          onChange={handleOnChange}
        >
          Без пересадок
        </Checkbox>
        <br />
        <Checkbox
          checked={!!(transfersFilter & TransfersFilterEnum.TRANSFERS_ONE)}
          value={TransfersFilterEnum.TRANSFERS_ONE}
          className={styles.checkbox}
          onChange={handleOnChange}
        >
          1 пересадка
        </Checkbox>
        <br />
        <Checkbox
          checked={!!(transfersFilter & TransfersFilterEnum.TRANSFERS_TWO)}
          value={TransfersFilterEnum.TRANSFERS_TWO}
          className={styles.checkbox}
          onChange={handleOnChange}
        >
          2 пересадки
        </Checkbox>
        <br />
        <Checkbox
          checked={!!(transfersFilter & TransfersFilterEnum.TRANSFERS_THREE)}
          value={TransfersFilterEnum.TRANSFERS_THREE}
          className={styles.checkbox}
          onChange={handleOnChange}
        >
          3 пересадки
        </Checkbox>
      </div>
    </div>
  )
}

export default TransfersFilter
