import React from "react"
import styles from "./Sidebar.module.css"
import TransfersFilter from "./TransfersFilter"

const Sidebar: React.FC = () => {
  return (
    <div className={styles.sidebar}>
      <TransfersFilter />
    </div>
  )
}

export default Sidebar
